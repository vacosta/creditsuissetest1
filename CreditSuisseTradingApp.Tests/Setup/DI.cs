﻿using System;
using Autofac;
using CreditSuisseTradingApp.ServiceLayer;

namespace CreditSuisseTradingApp.Tests.Setup
{
    public static class DI
    {

        /// <summary>
        /// Registers all the concrete implementations (at this stage we only have one), and provides an extraSetup method to override them with mocks
        /// </summary>
        /// <param name="extraSetup"></param>
        /// <returns></returns>
        public static IContainer Setup(Action<ContainerBuilder> extraSetup)
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<EquityOrder>().As<IEquityOrder>();
            extraSetup?.Invoke(builder);
            return builder.Build();
        }

    }
}
