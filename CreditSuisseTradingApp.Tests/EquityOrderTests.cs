﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Autofac.Core;
using CreditSuisseTradingApp.ServiceLayer;
using CreditSuisseTradingApp.Tests.Setup;
using CreditSuisseTradingApp.Tests.Stubs;
using CreditSuisseTradingApp.Tests.Stubs.HelperTypes;
using Moq;
using NUnit.Framework;

namespace CreditSuisseTradingApp.Tests
{

    [TestFixture]
    public class EquityOrderTests
    {
        private IContainer _iocContainer;



        [Test]
        [Description("TestCase for requirements 2 and 3.1")]
        public void Test_OrderService_Places_Order_At_The_Right_Price()
        {
            //ARRANGE
            bool orderPlaced = false;
            decimal orderValue = 0;
            var targetCriteria = new OrderCriteria() { Side = OrderSide.Buy, LimitPrice = 11, Quantity = 1 };
            var fakeOrderService = CreateMockOrderService((code, price) =>
            {
                orderPlaced = true;
                orderValue = price;
            });
            _iocContainer = DI.Setup(builder =>
            {
                //Step. Register our test-specific services
                builder.RegisterInstance(fakeOrderService).As<IOrderService>();
            });

            //Step. Fake market data  
            var marketData = new TestMarketData(new[]
            {
                StaticMarketData.EQUITY1_PRICE12,
                StaticMarketData.EQUITY1_PRICE11
            });
            //Step.  Generate one order
            var basketOfOrders = new TestBasket(new[]
            {
                CreateOrder(StaticEquityCodes.EQUITY1, targetCriteria.LimitPrice, targetCriteria.Quantity)
            });

            //Step. Register them to the external tick source
            var externalTickSource = new TestExternalTickSource(basketOfOrders, marketData);


            //ACT
            externalTickSource.Push();
            Assert.AreEqual(false, orderPlaced, "Order should not be placed at £12, execution is expected at £11");

            externalTickSource.Push();
            Assert.AreEqual(true, orderPlaced, "Order did not execute");
            Assert.AreEqual(11, orderValue);

        }




        [Test]
        [TestCase(false)]
        [TestCase(true)]
        [Description("TestCase for requirements 3.b and 4")]
        public void Test_EquityOrder_Events_Are_Raised(bool failOrder)
        {
            //ARRANGE
            bool orderPlaced = false, orderFailed = false;
            var targetCriteria = new OrderCriteria() { Side = OrderSide.Buy, LimitPrice = 12, Quantity = 1 };
            var fakeOrderService = CreateMockOrderService(onBuy:(code, price) =>
            {
                if (failOrder) throw new Exception("system unavailable");
            });
            _iocContainer = DI.Setup(builder => builder.RegisterInstance(fakeOrderService).As<IOrderService>());

            //Step. Generate test data  
            var marketData = new TestMarketData(new[] { StaticMarketData.EQUITY1_PRICE12 });
            var basketOfOrders = new TestBasket(new[]
            {
                CreateOrder(StaticEquityCodes.EQUITY1, targetCriteria.LimitPrice, targetCriteria.Quantity, order =>
                {
                    order.OrderPlaced += args => orderPlaced = true;
                    order.OrderErrored += args => orderFailed = true;
                })
            });


            //Step. Register them to the external tick source
            var externalTickSource = new TestExternalTickSource(basketOfOrders, marketData);


            //ACT
            externalTickSource.Push();
            Assert.IsTrue(orderPlaced | orderFailed, "Order was not processed");
            Assert.AreEqual(failOrder, orderFailed);
            Assert.AreEqual(!failOrder, orderPlaced);
        }




        [Test]
        [Description("Test case for requirement 3.c and 5 - single order placed only once")]
        public void Test_EquityOrder_Places_Order_Only_Once()
        {
            //ARRANGE
            int buyCounter = 0;
            var fakeOrderService = CreateMockOrderService(onBuy: (code, price) => buyCounter++);

            var targetCriteria = new OrderCriteria() { Side = OrderSide.Buy, LimitPrice = 12, Quantity = 1 };
            //var equityOrderEvaluator = new EquityOrderBuyEvaluator() { LimitPrice = 11 };
            _iocContainer = DI.Setup(builder =>
            {
                //Step. Register our test-specific services
                builder.RegisterInstance(fakeOrderService).As<IOrderService>();
            });

            //Step. Generate test data  
            var marketData = new TestMarketData(new[]
            {
                StaticMarketData.EQUITY1_PRICE12,
                StaticMarketData.EQUITY1_PRICE11,
                StaticMarketData.EQUITY1_PRICE12,
                StaticMarketData.EQUITY1_PRICE11
            });
            var basketOfOrders = new TestBasket(new[] {
                CreateOrder(StaticEquityCodes.EQUITY1, targetCriteria.LimitPrice, targetCriteria.Quantity)
            });


            //Step. Register them to the external tick source
            var externalTickSource = new TestExternalTickSource(basketOfOrders, marketData);


            //ACT
            while (externalTickSource.HasData())
                externalTickSource.Push();


            //ASSERT
            Assert.AreNotEqual(0, buyCounter, "Order was not placed");
            Assert.AreEqual(1, buyCounter, "Order should have been placed only once");

        }


        

        [Test]
        [Description("Test case for requirement 3.c and 5 - multiple orders simultaneously")]
        public void Test_EquityOrder_MultipleInstances_Places_Order_Only_Once()
        {
            //ARRANGE
            int buyEquityOneCounter = 0, buyEquityTwoCounter = 0;
            var fakeOrderService = CreateMockOrderService(onBuy: (code, price) =>
            {
                if (code == StaticEquityCodes.EQUITY1) buyEquityOneCounter++;
                if (code == StaticEquityCodes.EQUITY2) buyEquityTwoCounter++;
            });

            _iocContainer = DI.Setup(builder => builder.RegisterInstance(fakeOrderService).As<IOrderService>());


            //Step. Generate test data
            var marketData = new TestMarketData(new[]
            {
                StaticMarketData.EQUITY1_PRICE10, StaticMarketData.EQUITY1_PRICE11, StaticMarketData.EQUITY1_PRICE10, StaticMarketData.EQUITY1_PRICE11,
                StaticMarketData.EQUITY2_PRICE10, StaticMarketData.EQUITY2_PRICE11, StaticMarketData.EQUITY2_PRICE10, StaticMarketData.EQUITY2_PRICE11,
            });
            var basketOfOrders = new TestBasket(new[] {
                CreateOrder(StaticEquityCodes.EQUITY1, 10,  1),
                CreateOrder(StaticEquityCodes.EQUITY2, 11,  1)
                //NOTE:  For the purpose of this test, I will provide one of each for the equity codes and they should be executed only once.
                //       There are not enough requirements to determine whether one single equity code should be in a basket.  There are many possibilities here which can be discussed later.  I hope this is enough for now
            });

            var externalTickSource = new TestExternalTickSource(basketOfOrders, marketData);


            //ACT
            while (externalTickSource.HasData())
                externalTickSource.Push();


            //ASSERT
            Assert.AreEqual(1, buyEquityOneCounter, "There should be only one order placed for equity 1");
            Assert.AreEqual(1, buyEquityTwoCounter, "There should be only one order placed for equity 2");

        }
        




        private IEquityOrder CreateOrder(string code, decimal price, int quantity, Action<IEquityOrder> extraSetup = null)
        {
            //Note: EquityOrder could have less responsibilities to reduce the number of injected services and values
            var order = _iocContainer.Resolve<IEquityOrder>(new Parameter[]
            {
                new TypedParameter(typeof(OrderCriteria), new OrderCriteria() {LimitPrice = price, Quantity = quantity, Side = OrderSide.Buy}),
                new NamedParameter("equityCode", code), 
            });
            extraSetup?.Invoke(order);
            return order;
        }


        private IOrderService CreateMockOrderService(Action<string, decimal> onBuy)
        {
            var orderService = new Moq.Mock<IOrderService>();
            orderService.Setup(x => x.Buy(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<decimal>()))
                .Callback<string, int, decimal>((code, quantity, price) => onBuy(code, price));
            return orderService.Object;
        }

    }
}
