﻿using CreditSuisseTradingApp.Tests.Stubs.HelperTypes;

namespace CreditSuisseTradingApp.Tests.Stubs
{

    public class StaticMarketData
    {

        public static TestTick EQUITY1_PRICE10 = new TestTick() { EquityCode = StaticEquityCodes.EQUITY1, CurrentPrice = 10 };
        public static TestTick EQUITY1_PRICE11 = new TestTick() { EquityCode = StaticEquityCodes.EQUITY1, CurrentPrice = 11 };
        public static TestTick EQUITY1_PRICE12 = new TestTick() { EquityCode = StaticEquityCodes.EQUITY1, CurrentPrice = 12 };
        public static TestTick EQUITY1_PRICE13 = new TestTick() { EquityCode = StaticEquityCodes.EQUITY1, CurrentPrice = 13 };

        public static TestTick EQUITY2_PRICE10 = new TestTick() { EquityCode = StaticEquityCodes.EQUITY2, CurrentPrice = 10 };
        public static TestTick EQUITY2_PRICE11 = new TestTick() { EquityCode = StaticEquityCodes.EQUITY2, CurrentPrice = 11 };
        public static TestTick EQUITY2_PRICE12 = new TestTick() { EquityCode = StaticEquityCodes.EQUITY2, CurrentPrice = 12 };
        public static TestTick EQUITY2_PRICE13 = new TestTick() { EquityCode = StaticEquityCodes.EQUITY2, CurrentPrice = 13 };


    }
}
