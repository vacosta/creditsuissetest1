﻿using System.Collections.Generic;
using CreditSuisseTradingApp.ServiceLayer;

namespace CreditSuisseTradingApp.Tests.Stubs.HelperTypes
{
    /// <summary>
    /// Represents a group of orders (only to make the tests look more readable)
    /// </summary>
    public class TestBasket
    {
        public TestBasket(IEnumerable<IEquityOrder> orders)
        {
            this.Orders = orders;
        }
        public IEnumerable<IEquityOrder> Orders { get; }
    }
}