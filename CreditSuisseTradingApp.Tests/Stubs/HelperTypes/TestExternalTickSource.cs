﻿using System.Linq;
using CreditSuisseTradingApp.Tests.Stubs.HelperTypes;

namespace CreditSuisseTradingApp.Tests.Stubs
{
    //A simple implementation of an external tick source (it is controlled by the Push method)
    public class TestExternalTickSource
    {
        //List of orders 
        private readonly TestBasket _basket;

        //List of prices
        private readonly TestMarketData _incomingMarketData;

        public TestExternalTickSource(TestBasket basket, TestMarketData incomingMarketData)
        {
            _basket = basket;
            _incomingMarketData = incomingMarketData;
        }

        public bool HasData() => _incomingMarketData.SimulatedData.Any();

        public void Push()
        {
                
            if (_incomingMarketData.SimulatedData.Any())
            {
                var data = _incomingMarketData.SimulatedData.Dequeue();
                foreach (var order in _basket.Orders)
                {
                    order.ReceiveTick(data.EquityCode, data.CurrentPrice);
                }
            }
        }


    }
}
