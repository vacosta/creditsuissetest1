﻿using System.Collections.Generic;

namespace CreditSuisseTradingApp.Tests.Stubs.HelperTypes
{
    /// <summary>
    /// Represents an incoming value from the market data feed
    /// </summary>
    public class TestTick
    {
        public string EquityCode { get; set; }

        public decimal CurrentPrice { get; set; }

    }

    public class TestMarketData 
    {
        public TestMarketData(IEnumerable<TestTick> data)
        {
            this.SimulatedData = new Queue<TestTick>(data);
        }

        public Queue<TestTick> SimulatedData { get; }

    }

}