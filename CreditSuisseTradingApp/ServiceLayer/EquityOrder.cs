﻿using System;
using CreditSuisseTradingApp.ServiceLayer.Events;

namespace CreditSuisseTradingApp.ServiceLayer
{

    public interface IEquityOrder : IOrderPlaced, IOrderErrored
    {
        void ReceiveTick(string equityCode, decimal price);
    }





    public class EquityOrder: IEquityOrder
    {
        private readonly IOrderService _orderService;
        private readonly string _equityCode;
        private readonly OrderCriteria _criteria;


        public bool IsCompleted { get; private set; }

        public bool IsFaulted { get; private set; }

        public event OrderPlacedEventHandler OrderPlaced;

        public event OrderErroredEventHandler OrderErrored;


        public EquityOrder(IOrderService orderService, string equityCode, OrderCriteria criteria)
        {
            _orderService = orderService;
            _equityCode = equityCode;
            _criteria = criteria;
        }


        public void ReceiveTick(string equityCode, decimal price)
        {
            if (IsCompleted || IsFaulted || string.Equals(equityCode, this._equityCode, StringComparison.InvariantCultureIgnoreCase) == false)
            {
                return;
            }
            if (IsMatch(price))
            {
                try
                {
                    if (_criteria.Side == OrderSide.Buy)
                    {
                        //I didn't want to change the original interfaces, but OrderService can benefit of some refactoring later in order to remove this if statement
                        IsCompleted = true;
                        _orderService.Buy(this._equityCode, this._criteria.Quantity, price);
                        OrderPlaced?.Invoke(new OrderPlacedEventArgs(this._equityCode, price));
                    }
                }
                catch (Exception ex)
                {
                    IsCompleted = false;
                    IsFaulted = true;
                    OrderErrored?.Invoke(new OrderErroredEventArgs(this._equityCode, price, ex));
                }
            }
        }

        public bool IsMatch(decimal value)
        {
            if (_criteria.Side == OrderSide.Buy)
            {
                return value <= _criteria.LimitPrice;
            }
            return false;
        }
    }
}
