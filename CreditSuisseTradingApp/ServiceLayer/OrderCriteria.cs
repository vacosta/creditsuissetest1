﻿namespace CreditSuisseTradingApp.ServiceLayer
{

    public enum OrderSide
    {
        Buy,
        Sell
    }


    public class OrderCriteria
    {
        public OrderSide Side { get; set; }

        public int Quantity { get; set; }

        public decimal LimitPrice { get; set; }
    }
}