﻿namespace CreditSuisseTradingApp.ServiceLayer.Events
{

    public delegate void OrderErroredEventHandler(OrderErroredEventArgs e);

    public interface IOrderErrored
    {
        event OrderErroredEventHandler OrderErrored;
    }
}