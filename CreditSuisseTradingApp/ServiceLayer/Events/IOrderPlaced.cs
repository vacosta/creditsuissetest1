﻿namespace CreditSuisseTradingApp.ServiceLayer.Events
{

    public delegate void OrderPlacedEventHandler(OrderPlacedEventArgs e);


    public interface IOrderPlaced
    {
        event OrderPlacedEventHandler OrderPlaced;
    }
}